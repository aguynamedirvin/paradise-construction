<?php if(!defined('KIRBY')) exit ?>

username: irvin
email: irvindominguez1@gmail.com
password: >
  $2a$10$Z/WQJ8GfFbVI1TXVYq34n.OqPxeoyLyGW9ZS3D7QRA7f/PXPSVQSy
language: en
role: admin
history:
  - projects/backyard-cooking-station
  - projects/home-front-yard-flower-beds
  - projects/backyard-inside-cooking-station
  - projects/garden-wall
  - projects/restroom-remodeling
firstname: Irvin
lastname: Dominguez
