<?php if(!defined('KIRBY')) exit ?>

# 404 error blueprint

title: Error
pages: false
files: false

fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
    size:  large