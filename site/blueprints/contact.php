<?php if(!defined('KIRBY')) exit ?>

# contact blueprint

title: Contact
pages: false

deletable: false

fields:
  title:
    label: Title
    type:  text
  summary:
    label: Summary
    type:  textarea
  description:
    label: SEO Summary
    type: textarea
    buttons: false