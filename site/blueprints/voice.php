<?php if(!defined('KIRBY')) exit ?>

# voice blueprint

title: Voice
pages: false
preview: parent
files: false

fields:
  title: Name
    label: Title
    type:  text
  quote:
    label: Quote
    type:  textarea