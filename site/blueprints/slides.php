<?php if(!defined('KIRBY')) exit ?>

# slide blueprint

title: Slides
pages:
  template: slide
  sort:     title asc

deletable: false

files: false
fields:
  title:
    label: Title
    type:  text